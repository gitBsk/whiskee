package stepsDefinitions;


import com.alti.base.Driverutils;
import com.alti.common.ConfigReader;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AndroidLaunchTM extends Driverutils {

    @Given("^open the TM app$")
    public void open_the_TM_app() throws Throwable {

        System.out.println("Opening the App....");
        Driverutils.launchApplication("dummyurl","mobapp");

    }

    @Then("^set the location to Newyork$")
    public void set_the_location_to_Newyork() throws Throwable {

        System.out.println("App opened.....");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement location = driver.findElement(By.id("com.ticketmaster.mobile.android.na:id/location_field"));
        location.click();
        location.sendKeys("10001");
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
        MobileElement locationSuggestion = driver.findElement(By.id("com.ticketmaster.mobile.android.na:id/location_suggestion"));
        locationSuggestion.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        System.out.println("Location Set to NewYork");
        driver.quit();
    }
}


