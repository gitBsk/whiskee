#Author: Pruthvi, Checking the spaces issues.
Feature: Test to verify the API testing with wiremock


Scenario: List down all the favorites of a user, TrackingFavorites API
	Given A wiremock server is started for trackingFavorites service.
	Then Make a GET request for trackingFavorites and validate the response.
	Then shutdown the wiremock server for trackingFavorites service.
	
	
Scenario: Add an event as a favorite, OnboardingPushFavorite API.
	Given A wiremock server is started for addFavorite service.
	Then Make a POST request for addFavorites and validate the response.
	Then shutdown the wiremock server for addFavorite service.
	
	
Scenario: Remove an event as a favorite, OnboardingRemoveFavorite API.
	Given A wiremock server is started for removeFavorite service.
	Then Make a POST request for removeFavorites and validate the response.
	Then shutdown the wiremock server for removeFavorites service.