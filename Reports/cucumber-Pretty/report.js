$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("tm_favorite.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "#Author: rchandra@altimetrik.com"
    }
  ],
  "line": 3,
  "name": "Different Operation on Favrorite API",
  "description": "",
  "id": "different-operation-on-favrorite-api",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 7,
  "name": "Favoriting an Event",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event",
  "type": "scenario_outline",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "make a request with post method with \"\u003centityid\u003e\" and \"\u003cdevicetype\u003e\" and \"\u003csource\u003e\" and \"\u003chmacid\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "verify the response code should be \"\u003cresponsecode\u003e\"",
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "launch webApplication with url \"\u003curl\u003e\" and browser \"\u003cbrowser\u003e\"",
  "keyword": "Then "
});
formatter.examples({
  "line": 14,
  "name": "",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event;",
  "rows": [
    {
      "cells": [
        "entityid",
        "devicetype",
        "source",
        "hmacid",
        "responsecode",
        "url",
        "browser"
      ],
      "line": 16,
      "id": "different-operation-on-favrorite-api;favoriting-an-event;;1"
    },
    {
      "cells": [
        "K8vZ9171Jo7",
        "DESKTOP",
        "TMAPP",
        "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
        "200",
        "https://ticketmaster.com",
        "firefox"
      ],
      "line": 17,
      "id": "different-operation-on-favrorite-api;favoriting-an-event;;2"
    },
    {
      "cells": [
        "K8vZ9171Jo7",
        "DESKTOP",
        "TMAPP",
        "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
        "200",
        "https://ticketmaster.com",
        "chrome"
      ],
      "line": 18,
      "id": "different-operation-on-favrorite-api;favoriting-an-event;;3"
    },
    {
      "cells": [
        "K8vZ9171Jo7",
        "DESKTOP",
        "TMAPP",
        "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
        "200",
        "https://ticketmaster.com",
        "mobapp"
      ],
      "line": 19,
      "id": "different-operation-on-favrorite-api;favoriting-an-event;;4"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 17,
  "name": "Favoriting an Event",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event;;2",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "make a request with post method with \"K8vZ9171Jo7\" and \"DESKTOP\" and \"TMAPP\" and \"cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "verify the response code should be \"200\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "launch webApplication with url \"https://ticketmaster.com\" and browser \"firefox\"",
  "matchedColumns": [
    5,
    6
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "K8vZ9171Jo7",
      "offset": 38
    },
    {
      "val": "DESKTOP",
      "offset": 56
    },
    {
      "val": "TMAPP",
      "offset": 70
    },
    {
      "val": "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
      "offset": 82
    }
  ],
  "location": "TM_Favorite.make_a_request_with_post_method_with_and_and_and(String,String,String,String)"
});
formatter.result({
  "duration": 5191547146,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 36
    }
  ],
  "location": "TM_Favorite.verify_the_response_code_should_be(String)"
});
formatter.result({
  "duration": 1958139,
  "status": "passed"
});
formatter.match({
  "location": "TM_Favorite.verify_the_favouriteid()"
});
formatter.result({
  "duration": 8871927,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://ticketmaster.com",
      "offset": 32
    },
    {
      "val": "firefox",
      "offset": 71
    }
  ],
  "location": "TM_Favorite.launch_webApplication_with_url_and_browser(String,String)"
});
formatter.result({
  "duration": 15622124451,
  "status": "passed"
});
formatter.scenario({
  "line": 18,
  "name": "Favoriting an Event",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event;;3",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "make a request with post method with \"K8vZ9171Jo7\" and \"DESKTOP\" and \"TMAPP\" and \"cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "verify the response code should be \"200\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "launch webApplication with url \"https://ticketmaster.com\" and browser \"chrome\"",
  "matchedColumns": [
    5,
    6
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "K8vZ9171Jo7",
      "offset": 38
    },
    {
      "val": "DESKTOP",
      "offset": 56
    },
    {
      "val": "TMAPP",
      "offset": 70
    },
    {
      "val": "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
      "offset": 82
    }
  ],
  "location": "TM_Favorite.make_a_request_with_post_method_with_and_and_and(String,String,String,String)"
});
formatter.result({
  "duration": 1040715268,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 36
    }
  ],
  "location": "TM_Favorite.verify_the_response_code_should_be(String)"
});
formatter.result({
  "duration": 62155,
  "status": "passed"
});
formatter.match({
  "location": "TM_Favorite.verify_the_favouriteid()"
});
formatter.result({
  "duration": 7531888,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://ticketmaster.com",
      "offset": 32
    },
    {
      "val": "chrome",
      "offset": 71
    }
  ],
  "location": "TM_Favorite.launch_webApplication_with_url_and_browser(String,String)"
});
formatter.result({
  "duration": 4494556861,
  "status": "passed"
});
formatter.scenario({
  "line": 19,
  "name": "Favoriting an Event",
  "description": "",
  "id": "different-operation-on-favrorite-api;favoriting-an-event;;4",
  "type": "scenario",
  "keyword": "Scenario Outline"
});
formatter.step({
  "line": 8,
  "name": "make a request with post method with \"K8vZ9171Jo7\" and \"DESKTOP\" and \"TMAPP\" and \"cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7\"",
  "matchedColumns": [
    0,
    1,
    2,
    3
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "verify the response code should be \"200\"",
  "matchedColumns": [
    4
  ],
  "keyword": "Then "
});
formatter.step({
  "line": 10,
  "name": "verify the favouriteid",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "launch webApplication with url \"https://ticketmaster.com\" and browser \"mobapp\"",
  "matchedColumns": [
    5,
    6
  ],
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "K8vZ9171Jo7",
      "offset": 38
    },
    {
      "val": "DESKTOP",
      "offset": 56
    },
    {
      "val": "TMAPP",
      "offset": 70
    },
    {
      "val": "cfa5f674b3b92c1c105fcb0b96392167d82d8b077586c9bb59829ebd6d27dce7",
      "offset": 82
    }
  ],
  "location": "TM_Favorite.make_a_request_with_post_method_with_and_and_and(String,String,String,String)"
});
formatter.result({
  "duration": 916705239,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "200",
      "offset": 36
    }
  ],
  "location": "TM_Favorite.verify_the_response_code_should_be(String)"
});
formatter.result({
  "duration": 62186,
  "status": "passed"
});
formatter.match({
  "location": "TM_Favorite.verify_the_favouriteid()"
});
formatter.result({
  "duration": 7776851,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "https://ticketmaster.com",
      "offset": 32
    },
    {
      "val": "mobapp",
      "offset": 71
    }
  ],
  "location": "TM_Favorite.launch_webApplication_with_url_and_browser(String,String)"
});
formatter.result({
  "duration": 121534879,
  "error_message": "java.lang.NoClassDefFoundError: org/openqa/selenium/remote/AcceptedW3CCapabilityKeys\n\tat io.appium.java_client.remote.NewAppiumSessionPayload.\u003cclinit\u003e(NewAppiumSessionPayload.java:98)\n\tat io.appium.java_client.remote.AppiumCommandExecutor$1.createSession(AppiumCommandExecutor.java:175)\n\tat io.appium.java_client.remote.AppiumCommandExecutor.createSession(AppiumCommandExecutor.java:217)\n\tat io.appium.java_client.remote.AppiumCommandExecutor.execute(AppiumCommandExecutor.java:239)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)\n\tat io.appium.java_client.DefaultGenericMobileDriver.execute(DefaultGenericMobileDriver.java:42)\n\tat io.appium.java_client.AppiumDriver.execute(AppiumDriver.java:1)\n\tat io.appium.java_client.android.AndroidDriver.execute(AndroidDriver.java:1)\n\tat org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:219)\n\tat org.openqa.selenium.remote.RemoteWebDriver.\u003cinit\u003e(RemoteWebDriver.java:142)\n\tat io.appium.java_client.DefaultGenericMobileDriver.\u003cinit\u003e(DefaultGenericMobileDriver.java:38)\n\tat io.appium.java_client.AppiumDriver.\u003cinit\u003e(AppiumDriver.java:84)\n\tat io.appium.java_client.AppiumDriver.\u003cinit\u003e(AppiumDriver.java:94)\n\tat io.appium.java_client.android.AndroidDriver.\u003cinit\u003e(AndroidDriver.java:95)\n\tat com.alti.base.Driverutils.initWebDriver(Driverutils.java:82)\n\tat com.alti.base.Driverutils.launchApplication(Driverutils.java:33)\n\tat stepsDefinitions.TM_Favorite.launch_webApplication_with_url_and_browser(TM_Favorite.java:93)\n\tat ✽.Then launch webApplication with url \"https://ticketmaster.com\" and browser \"mobapp\"(tm_favorite.feature:11)\nCaused by: java.lang.ClassNotFoundException: org.openqa.selenium.remote.AcceptedW3CCapabilityKeys\n\tat java.net.URLClassLoader.findClass(URLClassLoader.java:382)\n\tat java.lang.ClassLoader.loadClass(ClassLoader.java:418)\n\tat sun.misc.Launcher$AppClassLoader.loadClass(Launcher.java:355)\n\tat java.lang.ClassLoader.loadClass(ClassLoader.java:351)\n\tat io.appium.java_client.remote.NewAppiumSessionPayload.\u003cclinit\u003e(NewAppiumSessionPayload.java:98)\n\tat io.appium.java_client.remote.AppiumCommandExecutor$1.createSession(AppiumCommandExecutor.java:175)\n\tat io.appium.java_client.remote.AppiumCommandExecutor.createSession(AppiumCommandExecutor.java:217)\n\tat io.appium.java_client.remote.AppiumCommandExecutor.execute(AppiumCommandExecutor.java:239)\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:601)\n\tat io.appium.java_client.DefaultGenericMobileDriver.execute(DefaultGenericMobileDriver.java:42)\n\tat io.appium.java_client.AppiumDriver.execute(AppiumDriver.java:1)\n\tat io.appium.java_client.android.AndroidDriver.execute(AndroidDriver.java:1)\n\tat org.openqa.selenium.remote.RemoteWebDriver.startSession(RemoteWebDriver.java:219)\n\tat org.openqa.selenium.remote.RemoteWebDriver.\u003cinit\u003e(RemoteWebDriver.java:142)\n\tat io.appium.java_client.DefaultGenericMobileDriver.\u003cinit\u003e(DefaultGenericMobileDriver.java:38)\n\tat io.appium.java_client.AppiumDriver.\u003cinit\u003e(AppiumDriver.java:84)\n\tat io.appium.java_client.AppiumDriver.\u003cinit\u003e(AppiumDriver.java:94)\n\tat io.appium.java_client.android.AndroidDriver.\u003cinit\u003e(AndroidDriver.java:95)\n\tat com.alti.base.Driverutils.initWebDriver(Driverutils.java:82)\n\tat com.alti.base.Driverutils.launchApplication(Driverutils.java:33)\n\tat stepsDefinitions.TM_Favorite.launch_webApplication_with_url_and_browser(TM_Favorite.java:93)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat cucumber.runtime.Utils$1.call(Utils.java:40)\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\n\tat cucumber.runtime.Utils.invoke(Utils.java:34)\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:38)\n\tat cucumber.runtime.StepDefinitionMatch.runStep(StepDefinitionMatch.java:37)\n\tat cucumber.runtime.Runtime.runStep(Runtime.java:300)\n\tat cucumber.runtime.model.StepContainer.runStep(StepContainer.java:44)\n\tat cucumber.runtime.model.StepContainer.runSteps(StepContainer.java:39)\n\tat cucumber.runtime.model.CucumberScenario.run(CucumberScenario.java:44)\n\tat cucumber.runtime.model.CucumberScenarioOutline.run(CucumberScenarioOutline.java:46)\n\tat cucumber.runtime.model.CucumberFeature.run(CucumberFeature.java:165)\n\tat cucumber.api.testng.TestNGCucumberRunner.runCucumber(TestNGCucumberRunner.java:63)\n\tat cucumber.api.testng.AbstractTestNGCucumberTests.feature(AbstractTestNGCucumberTests.java:21)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\n\tat sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)\n\tat sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\n\tat java.lang.reflect.Method.invoke(Method.java:498)\n\tat org.testng.internal.MethodInvocationHelper.invokeMethod(MethodInvocationHelper.java:133)\n\tat org.testng.internal.TestInvoker.invokeMethod(TestInvoker.java:584)\n\tat org.testng.internal.TestInvoker.invokeTestMethod(TestInvoker.java:172)\n\tat org.testng.internal.MethodRunner.runInSequence(MethodRunner.java:46)\n\tat org.testng.internal.TestInvoker$MethodInvocationAgent.invoke(TestInvoker.java:804)\n\tat org.testng.internal.TestInvoker.invokeTestMethods(TestInvoker.java:145)\n\tat org.testng.internal.TestMethodWorker.invokeTestMethods(TestMethodWorker.java:146)\n\tat org.testng.internal.TestMethodWorker.run(TestMethodWorker.java:128)\n\tat java.util.ArrayList.forEach(ArrayList.java:1257)\n\tat org.testng.TestRunner.privateRun(TestRunner.java:770)\n\tat org.testng.TestRunner.run(TestRunner.java:591)\n\tat org.testng.SuiteRunner.runTest(SuiteRunner.java:402)\n\tat org.testng.SuiteRunner.runSequentially(SuiteRunner.java:396)\n\tat org.testng.SuiteRunner.privateRun(SuiteRunner.java:355)\n\tat org.testng.SuiteRunner.run(SuiteRunner.java:304)\n\tat org.testng.SuiteRunnerWorker.runSuite(SuiteRunnerWorker.java:53)\n\tat org.testng.SuiteRunnerWorker.run(SuiteRunnerWorker.java:96)\n\tat org.testng.TestNG.runSuitesSequentially(TestNG.java:1180)\n\tat org.testng.TestNG.runSuitesLocally(TestNG.java:1102)\n\tat org.testng.TestNG.runSuites(TestNG.java:1032)\n\tat org.testng.TestNG.run(TestNG.java:1000)\n\tat org.apache.maven.surefire.testng.TestNGExecutor.run(TestNGExecutor.java:135)\n\tat org.apache.maven.surefire.testng.TestNGDirectoryTestSuite.executeSingleClass(TestNGDirectoryTestSuite.java:112)\n\tat org.apache.maven.surefire.testng.TestNGDirectoryTestSuite.execute(TestNGDirectoryTestSuite.java:99)\n\tat org.apache.maven.surefire.testng.TestNGProvider.invoke(TestNGProvider.java:146)\n\tat org.apache.maven.surefire.booter.ForkedBooter.invokeProviderInSameClassLoader(ForkedBooter.java:379)\n\tat org.apache.maven.surefire.booter.ForkedBooter.runSuitesInProcess(ForkedBooter.java:340)\n\tat org.apache.maven.surefire.booter.ForkedBooter.execute(ForkedBooter.java:125)\n\tat org.apache.maven.surefire.booter.ForkedBooter.main(ForkedBooter.java:413)\n",
  "status": "failed"
});
formatter.scenario({
  "line": 22,
  "name": "list all the favorite from my favorite page",
  "description": "",
  "id": "different-operation-on-favrorite-api;list-all-the-favorite-from-my-favorite-page",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 23,
  "name": "make an GET Api Request",
  "keyword": "Given "
});
formatter.step({
  "line": 24,
  "name": "List all the events",
  "keyword": "Then "
});
formatter.match({
  "location": "TM_Favorite.make_an_GET_Api_Request()"
});
formatter.result({
  "duration": 783205721,
  "status": "passed"
});
formatter.match({
  "location": "TM_Favorite.list_all_the_events()"
});
formatter.result({
  "duration": 26347386,
  "status": "passed"
});
});